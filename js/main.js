
App = Ember.Application.create({});


/*************************************************
 * Ember Models
 *
 *************************************************/

App.Designer = DS.Model.extend({
  firstName:DS.attr('string'),
  lastName:DS.attr('string'),
  portfolio :DS.attr('string'),
  email:DS.attr('string'),
  note:DS.attr('string')
});


/*************************************************
 *Ember Fixtures *
 *************************************************/

var designers = [
  {
  
  id:1,
  firstName:'Brittany',
  lastName:'Arnett',
  portfolio :'http://www.brittanyarnett.com',
  email:'https://iu.box.com/shared/static/skaat5naynbvme42hmq0.pdf',
  note:'Passionate about designing educational experiences that create knowledge acquisition in interactive and fun ways through games & technology.',
  image:'img/designers/arnett.jpg'

},

{
 
  id:2,
  firstName:'Alisa',
  lastName:'Avigan',
  portfolio :'http://www.alisaavigan.com',
  email:'https://iu.box.com/shared/static/6dvmynfxf5v8ep265c35.pdf',
  note:'As technology-mediated person-to-person and person-to-environment interactions increase, I strive to make such interactions more meaningful.',
  image:'img/designers/avigan.jpg' 

},  

 {
  
  id:3,
  firstName:'Yao',
  lastName:'Bao',
  portfolio :'http://www.bao-yao.com',
  email:'https://iu.box.com/shared/static/7fobhn08mrkk9irs14tf.pdf',
  note:'Do’er and thinker. Interaction designer - Love novelty, love music, love sports, and love my life. Trying to make life better.',
  image:'img/designers/bao.jpg',
  direction:'toleft'


},

  {
  

  id:4,
  firstName:'Deepak',
  lastName:'Bhagchandani',
  portfolio :'http://www.deepakbhagchandani.com',
  email:'https://iu.box.com/shared/static/vgb90f4eg8wi3sy7pudz.pdf',
  note:'Designer + Developer interested in helping people tackle real life problems through human centered design.',
  image:'img/designers/bhagchandani.jpg'



},

  {
  
  id:5,
  firstName:'Sarang',
  lastName:'Borude',
  portfolio :'http://www.borude.com',
  email:'https://iu.box.com/shared/static/16xmh78madzmq7u45t15.pdf',
  note:'With great power comes great responsibility. Design is my power designing for people is my responsibility.',
  image:'img/designers/borude.jpg'


},

  {
  
  id:6,
  firstName:'JerNettie',
  lastName:'Burney',
  portfolio :'',
  email:'https://iu.box.com/shared/static/3df886s9olidaf89jzh3.pdf',
  note:'I absolutely adore being challenged to design creatively, coming up with new ways of approaching a problem and interacting with others!',
  image:'img/designers/burney.jpg',
  direction:'toleft'
  

},

 {
  

  id:7,
  firstName:'Chi',
  lastName:'Chen',
  portfolio :'http://www.thinkermaker.me',
  email:'https://iu.box.com/shared/static/ospsk55ijbpzviu2roc0.pdf',
  note:'Look into the reality. Think outside the box. Make from the heart.',
  image:'img/designers/chen.jpg'

},

  {
 
  id:8,
  firstName:'Kudzai',
  lastName:'Chinyadza',
  portfolio :'http://kudzaichinyadza.com/',
  email:'https://iu.box.com/shared/static/mnohcac8l1ipc1lpwm6g.pdf',
  note:'I have lots of questions. I believe that good design is a direct response to good questions..',
  image:'img/designers/chinyadza.jpg'

},

  {
  
  id:9,
  firstName:'Corrie',
  lastName:'Colombero',
  portfolio :'http://www.corriecolombero.com',
  email:'https://iu.box.com/shared/static/wgcmcw9d0hko4s29yunp.pdf',
  note:'My life experience contributes to my design thinking. Design is an adventure. Adventure is my life.',
  image:'img/designers/colombero.jpg', 
  direction:'toleft'

},

{
  id:10,
  firstName:'Wenyang',
  lastName:'Dong',
  portfolio :'http://www.wenyangdong.com',
  email:'https://iu.box.com/shared/static/lasnsucamkeflnxna1ng.pdf',
  note:'I look for the nature of problem and try to address the essences.',
  image:'img/designers/dong.jpg'

},

  {
  id:11,
  firstName:'Gene',
  lastName:'Dreyband',
  portfolio :'http://www.genedreyband.com',
  email:'https://iu.box.com/shared/static/yjkpr8l95vc5nogynh8q.pdf',
  note:'I’m an interaction designer interested in education, systems thinking and urban planning. I live to help others reach their potential.',
  image:'img/designers/dreyband.jpg'

},

  {
  
  id:12,
  firstName:'Dennis',
  lastName:'Ellis',
  portfolio :'http://www.dellisdesign.com',
  email:'https://iu.box.com/shared/static/vzk5lsb9fr1y8tlj6fzy.pdf',
  note:'I’m a deep thinker who loves to solve complicated problems. I prefer a meaningful, useful, and sustainable solution to a fancy one.',
  image:'img/designers/ellis.jpg',
  direction:'toleft'

},

{

  id:13,
  firstName:'Emma',
  lastName:'Fagergren',
  portfolio :'http://www.emmafagergren.com',
  email:'https://iu.box.com/shared/static/wr7nha5emrgatiy2cbft.pdf',
  note:'UX designer and Swedish import. DIY-er, doodler and dancer. Always learning, constantly making. Looking for a great team to join.',
  image:'img/designers/fagergren.jpg'

},

{
  
  id:14,
  firstName:'Denique',
  lastName:'Ferguson',
  portfolio :'http://www.denferguson.com',
  email:'https://iu.box.com/shared/static/ft49kayyps4x3mw3nxqx.pdf',
  note:'Foundation in behavior change communication, experience in business analysis, designing for the dialogues between people and their worlds.',
  image:'img/designers/ferguson.jpg'


},

  {
  
  id:15,
  firstName:'Jared',
  lastName:'Forney',
  portfolio :'http://www.jaredforney.com',
  email:'https://iu.box.com/shared/static/u61ws8e6bk2ompkstz6t.pdf',
  note:'I believe design is a conversation between designers, stakeholders, users, and the environment that links them together. So let’s talk!',
  image:'img/designers/forney.jpg',
  direction:'toleft'

},

  {
  
  id:16,
  firstName:'Jason',
  lastName:'Fu',
  portfolio :'http://www.fuyihe.com',
  email:'https://iu.box.com/shared/static/3rxzzlr1em6i6m6282qc.pdf',
  note:'A UX designer. An idealistic practitioner. A watcher with childlike curiosity. A searcher for little life wonders.',
  image:'img/designers/fu.jpg' 
  
},


{
  
  id:17,
  firstName:'Jeffrey',
  lastName:'Gadzala',
  portfolio :'http://www.jeffreygadzala.com',
  email:'https://iu.box.com/s/fy8xt4s6n8py8doznbm2',
  note:'I am a designer passionate about digital literacy and awesome experiences that help people learn. Grid enthusiast and hockey fanatic.',
  image:'img/designers/gadzala.jpg'


},

  {
  

  id:18,
  firstName:'Manali',
  lastName:'Gortekar',
  portfolio :'http://www.manaligortekar.com',
  email:'https://iu.box.com/shared/static/2zqo7t06r8rqjdjkebjj.pdf',
  note:'A firm believer of ‘small wins’. As a designer, I want to better align technology with people.',
  image:'img/designers/gortekar.jpg',
  direction:'toleft'



},

  {
  
  id:19,
  firstName:'Urvashi',
  lastName:'Gupta',
  portfolio :'http://www.urvashigupta.com',
  email:'https://iu.box.com/shared/static/6p9b9um6psj9wikg0n8c.pdf',
  note:'My Mantra: Live life to the fullest. I’m an aspiring interaction and experience designer and I dream to change the world through design.',
  image:'img/designers/gupta.jpg'

},

{

  id:20,
  firstName:'Craig',
  lastName:'Harkness',
  portfolio :'http://www.craigharkness.com',
  email:'https://iu.box.com/shared/static/mah5i6zorns375szn1cu.pdf',
  note:'British designer with a passion for travelling trying to broaden my perspective on life and design.',
  image:'img/designers/harkness.jpg'

},

{
  id:21,
  firstName:'Jordan',
  lastName:'Hayes',
  portfolio :'http://www.jhdesign.info',
  email:'https://iu.box.com/shared/static/bzr65l7f1zh723xb8a3v.pdf',
  note:'Great design forms connections, answers the question why, and is human-centered. My tools: sketching, scenarios, observation, empathy...',
  image:'img/designers/hayes.jpg',
  direction:'toleft'

},

{
  
  id:22,
  firstName:'Stephen',
  lastName:'Hicks',
  portfolio :'http://www.uxstoryteller.com',
  email:'https://iu.box.com/shared/static/diuswf3n9c776sw3t4lr.pdf',
  note:'Stephen is a second-year HCI/d student and works with SOIC Career Services. He is excited to start work as a UX researcher with Microsoft.',
  image:'img/designers/hicks.jpg'

},

{
 
  id:23,
  firstName:'Qian',
  lastName:'Huang',
  portfolio :'http://www.qian-huang.com',
  email:'https://iu.box.com/shared/static/g6nst7c5810pmwwz9n05.pdf',
  note:'As an illegitimate love-child of usability and aesthetic, I might design things neither useful nor pretty. But always care enough to try.',
  image:'img/designers/huang.jpg'
  

},

{
  
  id:24,
  firstName:'Alex',
  lastName:'Hughes',
  portfolio :'http://www.aahughes.com',
  email:'https://iu.app.box.com/s/6qv08cudgcvgom7cefrd',
  note:'Creative thinker, team player, and passionate designer striving to make amazing things with a group of innovative people.',
  image:'img/designers/hughes.jpg',
  direction:'toleft'


},

{
  
  id:25,
  firstName:'Ekaterina',
  lastName:'Ivanova',
  portfolio :'http://www.eivanova.com',
  email:'http://eivanova.com/Ivanova_Ekaterina_Resume_Dec2013.pdf',
  note:'Intuitive. Creative. Strives for efficiency and simplicity. Smart, getting smarter. From DC with a background in healthcare and psychology.',
  image:'img/designers/ivanova.jpg'

  
},

{
  
  id:26,
  firstName:'Jordan',
  lastName:'Jalles',
  portfolio :'http://www.jordanjalles.com',
  email:'https://iu.box.com/shared/static/lon5re7g5lkzrnd9q9oi.pdf',
  note:'Fast learner looking to work on the next big thing. Teamwork, coding and digital art. I have always been into playful computer creations.',
  image:'img/designers/jalles.jpg'

},

{
  
  id:27,
  firstName:'Tiffany',
  lastName:'Jen',
  portfolio :'http://www.tiffany-jen.com',
  email:'https://iu.box.com/shared/static/a7bvn2jnt1codrkaduoh.pdf',
  note:'I believe interaction design is not just between human and computer. It’s also leveraging technologies to create bonds between people.',
  image:'img/designers/jen.jpg',
  direction:'toleft'


},

{
  
  id:28,
  firstName:'Matt',
  lastName:'Jennex',
  portfolio :'http://www.mattjennex.com',
  email:'https://iu.box.com/shared/static/cns4lpcb5mtd6k3ynie2.pdf',
  note:'The world is filled with wondrous facts and experiences that I want to collect, and design gives me an outlet to apply that collection.',
  image:'img/designers/jennex.jpg' 

  
},


{
 
  id:29,
  firstName:'Tony',
  lastName:'Kennedy',
  portfolio :'http://www.experiencetk.com',
  email:'https://iu.box.com/shared/static/84lut4v08jrrw3eswzmb.pdf',
  note:'I’m a systems thinker who strives to bring out the best in groups I work with. I’m your phone a friend for all things music and sports.',
  image:'img/designers/kennedy.jpg'


},

{
 
  id:30,
  firstName:'Jiaqi',
  lastName:'Li',
  portfolio :'http://www.jiaqi-li.com',
  email:'https://iu.box.com/shared/static/l3yjqdkany0q9xpmu30p.pdf',
  note:'I’m a growing designer who love to explore what our life should be like. Design for living ‘healthier’, more meaningful!',
  image:'img/designers/lijiaqi.jpg',
  direction:'toleft'

},

{
 
id:31,
  firstName:'Tianjie',
  lastName:'Li',
  portfolio :'http://www.tianjieli.com',
  email:'https://iu.box.com/shared/static/kgm30ev528rdkso33crs.pdf',
  note:'Good design sometimes can be just a little change. No one thinks of it or even notices it, but once improved, a big step forward.',
  image:'img/designers/litianjie-.jpg'

  
},

{
  
id:32,
  firstName:'Yisi',
  lastName:'Lin',
  portfolio :'http://www.linyisi.com',
  email:'https://iu.box.com/shared/static/7lz6unkqnzkzwq4qk4tl.pdf',
  note:'My life is a passionate interaction between myself and computer, music, traveling and design.',
  image:'img/designers/lin.jpg'


},

{
  
  id:33,
  firstName:'Stephanie',
  lastName:'Louraine',
  portfolio :'http://www.slouraine.com',
  email:'https://iu.box.com/shared/static/uckrndi8lnupbxiyuvl4.pdf',
  note:'I’m a UX designer with a passion for user research. I start conversations. I learn about people to design stuff they love!',
  image:'img/designers/louraine.jpg',
  direction:'toleft'

},

{
  

  id:34,
  firstName:'Xuan',
  lastName:'Luo',
  portfolio :'http://www.exldesigner.com',
  email:'https://iu.box.com/shared/static/uwc8u85emklbwc3j51ll.pdf',
  note:'Design to me is first about self-exploration, and then finding the marvellous connection between my user and I.',
  image:'img/designers/luo.jpg' 

  
},

{
 
  id:35,
  firstName:'Shruti',
  lastName:'Meshram',
  portfolio :'http://www.shrutimeshram.com',
  email:'https://iu.box.com/shared/static/qfrrqhv0emspufu7awvs.pdf',
  note:'A UX / interaction designer who wants to create something which makes people happy, excited and productive.',
  image:'img/designers/meshram.jpg'


},

{

  id:36,
  firstName:'Liz',
  lastName:'Mikolaj',
  portfolio :'http://www.mikoliz.net',
  email:'https://iu.box.com/shared/static/1z2kzcg5wgocf58eaqq8.pdf',
  note:'A crafter/former engineer who believes empathy and human to human interaction are at the core of design. Lets take tech beyond the screen!',
  image:'img/designers/mikolaj.jpg', 
  direction:'toleft'

},

{
   
  id:37,
  firstName:'Stephen',
  lastName:'Miller',
  portfolio :'http://www.stephenfmiller.com',
  email:'https://iu.box.com/shared/static/6cydmq52visarj99ugqf.pdf',
  note:'I’m second year looking for full time employment to begin June 2014. Be kind, and work hard.',
  image:'img/designers/miller.jpg'

  
},


{
  id:38,
  firstName:'Pui',
  lastName:'Mo',
  portfolio :'http://www.puimo.com',
  email:'https://iu.box.com/shared/static/aal5mhd2t2uf02h272am.pdf',
  note:'A creative storyteller, a sensitive life composer and an adventurous explorer who work to change, to inspire and to touch.',
  image:'img/designers/mo.jpg'
  

},


{
  id:39,
  firstName:'Michael',
  lastName:'Moreau',
  portfolio :'http://www.michaelcohnmoreau.com',
  email:'https://iu.box.com/shared/static/slqb4pv4502cb9y7zus5.pdf',
  note:'I’m a designer developer. Coming from software, my goal in studying interaction design is to gain the skills to build better programs.',
  image:'img/designers/moreau.jpg',
  direction:'toleft'

},


{
  
id:40,
  firstName:'Zan',
  lastName:'Morris',
  portfolio :'http://www.zanmorris.me',
  email:'https://iu.box.com/shared/static/qvdje4d8wlhek0okf6nu.pdf',
  note:'Harmonizing Design Methods with a Developer Background to explore ethical questions and affect social change through playful design.',
  image:'img/designers/morris.jpg' 

  
},


{
  

  id:41,
  firstName:'Clark',
  lastName:'Mullen',
  portfolio :'http://www.clarkmullen.com',
  email:'https://iu.box.com/shared/static/51k1wpfb6qz7brnm0z79.pdf',
  note:'UX designer with computer science background. Cyclist, foodie, and musician. Passionate about great design. Want to be part of an awesome team.',
  image:'img/designers/mullen.jpg'



},


{
  

  id:42,
  firstName:'Christopher',
  lastName:'Myles',
  portfolio :'http://www.cjmyles.com',
  email:'https://iu.box.com/shared/static/p951zylf5q7f27zmgs86.pdf',
  note:'I’m striving toward a place in life that allows myself to pursue my passions as a career. Exploring every opportunity that comes my way.',
  image:'img/designers/myles.jpg',
  direction:'toleft'



},


{
  

  id:43,
  firstName:'Vamsi',
  lastName:'Pasupuleti',
  portfolio :'http://www.vamsichaitanya.com',
  email:'https://iu.box.com/shared/static/g61tj7gknl1j3n24zx4g.pdf',
  note:'Pursing design was coincidence and over the years I realized that it is not just about problem solving, it is a responsibility to inform user.',
  image:'img/designers/chaitanya.jpg' 

},


{
  
  id:44,
  firstName:'Gabe',
  lastName:'Persons',
  portfolio :'http://www.gabepersons.com',
  email:'https://iu.box.com/shared/static/k3j1wx28xnbe1zkll0up.pdf',
  note:'Storyteller and gamer who loves to learn. I’m interested in using playful design and game elements to explore social group dynamics.',
  image:'img/designers/persons.jpg'

  
},


{
  
  id:45,
  firstName:'Stephanie',
  lastName:'Poppe',
  portfolio :'http://www.whoispoppe.info',
  email:'https://iu.box.com/s/urv1l0w9meo8y8il8eye',
  note:'I love solving real human problems using a proper process and a critical eye, a sprinkle of playfulness, and always asking why.',
  image:'img/designers/poppe.jpg',
  direction:'toleft'


},

{
  
id:46,
  firstName:'Nathan',
  lastName:'Potts',
  portfolio :'http://www.nathancpotts.com',
  email:'https://iu.box.com/shared/static/gt6a8zwdq9x53sruin1o.pdf',
  note:'I’m a user experience designer who believes great design involves great storytelling, great experiences, and a pinch of code.',
  image:'img/designers/potts.jpg'


},


{
  

  id:47,
  firstName:'Anusha',
  lastName:'Radhakrishnan',
  portfolio :'http://www.ace-anusha.com',
  email:'https://iu.box.com/shared/static/rr8hn58xip06tindj8ta.pdf',
  note:'Flying over oceans and hills to make it big as a UX designer, while keeping abreast of design trends and user needs.',
  image:'img/designers/radhakrishnan.jpg'
  
  
},

{

  id:48,
  firstName:'Colson',
  lastName:'Rice',
  portfolio :'http://forgeux.com/',
  email:'https://iu.box.com/s/2d1kah60sqa7zpn2kvau',
  note:'I want to put the power in the users’ hands. When someone says, "That would be cool if..." I want to be the person that makes it happen.',
  image:'img/designers/rice.jpg',
  direction:'toleft'


},

{

  id:49,
  firstName:'Julia',
  lastName:'Rickles',
  portfolio :'http://www.juliarickles.com',
  email:'https://iu.box.com/shared/static/mhty75ob5jpgdckzmpx8.pdf',
  note:'I’m a graphic designer turned interaction designer. I hope to work for a company that creates sublime experiences for users.',
  image:'img/designers/rickles-.jpg'

},



{
  

  id:50,
  firstName:'Ang&Eacutelica',
  lastName:'Rosenzweig',
  portfolio :'http://www.arcpato.com',
  email:'https://iu.box.com/shared/static/lqc5hrj0adi77pusvhih.pdf',
  note:'Adaptable, analytical, a design chameleon; I design memorable experiences. Interactive installations, museums, and Broadway inspire me.',
  image:'img/designers/rosenzweig.jpg'


  
},

{
  id:51,
  firstName:'Mon&Eacutet',
  lastName:'Rouse',
  portfolio :'http://www.monetrouse.com',
  email:'https://iu.box.com/shared/static/w3xnw940zfjtjjjgkddy.pdf',
  note:'Every design is a story and I aim to be an author for the users’ vision. Dreamer and Doer. Mover and Shaker. Ideator and Creator.',
  image:'img/designers/rouse.jpg',
  direction:'toleft'

},

{
  


  id:52,
  firstName:'Sourjya Sinha',
  lastName:'Roy',
  portfolio :'http://www.sourjyasinharoy.com',
  email:'https://iu.box.com/shared/static/hea6hucx4wdy7mtag12d.pdf',
  note:'New perspectives & exposure to different cultures help me create playful designs. I am a designer, musician, martial artist and stuntman.',
  image:'img/designers/sinha.jpg' 

},


{
  
  id:53,
  firstName:'Sai Shivani',
  lastName:'Soundararaj',
  portfolio :'http://www.ssaishivani.com',
  email:'https://www.dropbox.com/s/ngssup7g16krbcq/SaiShivani_Resume.pdf',
  note:'I believe that design is not about creating interfaces, but creating the right experience. I strive to design memorable user experiences.',
  image:'img/designers/soundararaj.jpg' 
  

},

{

  id:54,
  firstName:'Mitch',
  lastName:'Spicer',
  portfolio :'http://www.mitchspicer.com',
  email:'https://iu.box.com/shared/static/s6826dltrx7zibdt14cs.pdf',
  note:'Background in Informatics, Telecommunications, and German, experience in politics, focus on design as a mode of transparency and broadcast.',
  image:'img/designers/spicer.jpg',
  direction:'toleft'


},


{
  
  id:55,
  firstName:'Michael',
  lastName:'Stallings',
  portfolio :'http://www.michaelstallings.me',
  email:'https://iu.box.com/shared/static/qgd40m0xavwyhl4iamck.pdf',
  note:'I believe in computer imagination, meaningful UX, and always being a student of one’s craft. I’ll join Blackbaud this July as a UX designer.',
  image:'img/designers/stallings.jpg'

},


{
  
  id:56,
  firstName:'Melissa',
  lastName:'Tang',
  portfolio :'http://boiled-water.com',
  email:'https://iu.box.com/shared/static/1nxihg8m0gabv5pyibqo.pdf',
  note:'Enjoying exploration and optimization, I like technology and desire to use technology to change people’s life. I am empathetic and brave.',
  image:'img/designers/tang.jpg'
  
},


{
  
  id:57,
  firstName:'Steve',
  lastName:'Tularak',
  portfolio :'http://www.stevetularak.com',
  email:'http://stevetularak.com/img/experience-resume/st-resume.pdf',
  note:'Design Is—Relevant, human and accessible. It’s creating meaningful connections between people that can be realized through products and services.',
  image:'img/designers/tularak.jpg',
  direction:'toleft'


},


{
  
  id:58,
  firstName:'Steve',
  lastName:'Voyk',
  portfolio :'http://www.stevenvoyk.com',
  email:'https://www.dropbox.com/s/2jmcffdcsycgwk3/Voyk%20Resume.pdf',
  note:'I am a technician, a martial artist, a bio-hacker, and now I am evolving into a designer.',
  image:'img/designers/voyk-.jpg'


},

{
  id:59,
  firstName:'LuLu',
  lastName:'Wang',
  portfolio :'http://www.thoughtfulsenses.com',
  email:'https://iu.box.com/shared/static/qomnuby20qalyhflmc0z.pdf',
  note:'I have a background of economics and business and I am passionate about gestural-based and mobile technology that improves people’s lives.',
  image:'img/designers/wang.jpg'
  
},

{
  
  id:60,
  firstName:'Adam',
  lastName:'Williams',
  portfolio :'http://www.awilliamsid.com',
  email:'https://iu.box.com/shared/static/54p89hxrofqgm4wdu0xq.pdf',
  note:'Design has been a cornerstone of my life. I continue to sculpt it daily. The core of my philosophy is defined by simplicity and honesty.',
  image:'img/designers/williams.jpg',
  direction:'toleft'



},

  {
  

  id:61,
  firstName:'Joel',
  lastName:'Wisneski',
  portfolio :'http://www.joelwisneski.com',
  email:'https://iu.box.com/shared/static/w0ay1dycotbx0lv7684r.pdf',
  note:'Art + code + design. These lenses assist my experimentation with existing and emerging technology that I use to inspire creative expression.',
  image:'img/designers/wisneski.jpg' 


},

  {
  

  id:62,
  firstName:'Sam',
  lastName:'Xia',
  portfolio :'http://mypointofview.info',
  email:'http://static.squarespace.com/static/50c04169e4b08e89974e3bda/t/52d9baa6e4b04fa13a217a2d/1390000806406/Resume_TX.pdf',
  note:' I’m interested in design and photography. Topics like experience design, graphics, visual thinking, and digital imagery making are my favorite.',
  image:'img/designers/xia.jpg'

  
},

{
   
id:63,
  firstName:'Ellie',
  lastName:'Xu',
  portfolio :'http://www.xuyudesign.com',
  email:'https://iu.box.com/shared/static/zy8wmt9g07mvtyyn3kw2.pdf',
  note:'Stay elegant. Stay simple.',
  image:'img/designers/xu.jpg',
  direction:'toleft'


},

  {
  
id:64,
  firstName:'Sijie',
  lastName:'Yang',
  portfolio :'http://www.sijieyang.com',
  email:'https://iu.box.com/shared/static/uokpiqt3gcyvezfuk71e.pdf',
  note:'As a skillful & reflective interaction/UX/UI designer, Sijie is characterized by empathy, input, responsibility, consistency & restorative.',
  image:'img/designers/yangcj.jpg'


},

  {

  id:65,
  firstName:'Yishi',
  lastName:'Yang',
  portfolio :'http://www.yishiyang.info',
  email:'https://dl.dropboxusercontent.com/u/102387401/YishiYang-resume.pdf',
  note:'Good design is a mapping from the designers heart into reality. I want to design a better world with my heart.',
  image:'img/designers/ysyang.jpg' 
  
},


{

  id:66,
  firstName:'Yalu',
  lastName:'Ye',
  portfolio :'http://www.yaluye.info',
  email:'https://iu.box.com/shared/static/zvyx449xrrmpzcm7j8ri.pdf',
  note:'sea + hiking + adventure + endless why + sci-fi + design & tech + passion + social impact + make life better + experience life = part of me',
  image:'img/designers/ye.jpg',
  direction:'toleft'

},

  {
  
  id:67,
  firstName:'Thai',
  lastName:'Yue',
  portfolio :'http://www.thaiyue.com',
  email:'https://iu.box.com/shared/static/rzc7gp4epljck830hk17.pdf',
  note:'I have been graciously humbled, bent but not broken, and taught by the finest teachers of UX design in my generation. How’s that for a tweet',
  image:'img/designers/yue.jpg'


},

  {
  
  id:68,
  firstName:'Ke',
  lastName:'Zhang',
  portfolio :'http://www.ke-zhang.net',
  email:'https://iu.box.com/shared/static/t9sz7stkgsiqhdckae7e.pdf',
  note:'Passionate but calm. Love painting, love design. I am always open to learning new skills and believe in making a difference through design.',
  image:'img/designers/zhang.jpg' 

  
},

{

  id:69,
  firstName:'Rayne',
  lastName:'Zhou',
  portfolio :'',
  email:'https://iu.box.com/shared/static/09u6ns78f9i0jnvymrwa.pdf',
  note:'I design myself. I design my own life. I’m a designer of design.',
  image:'img/designers/zhou-.jpg',
  direction:'toleft'
 
},

{

  id:70,
  firstName:'Yue',
  lastName:'Pan',
  portfolio :'http://www.panyuetia.com',
  email:'http://www.linkedin.com/in/panyue',
  note:'Yue is a design researcher who cares about people, design and the experiences.',
  image:'img/designers/pan.jpg'
 
},

/*

  {
  id:68,
  firstName:'',
  lastName:'',
  portfolio :'',
  email:'',
  note:'',
  image:''

},

  {
  id:69,
  firstName:'',
  lastName:'',
  portfolio :'',
  email:'',
  note:'',
  image:'',
  direction:'toleft'
  
},
*/








];


/*************************************************
 * Ember Routing 
 *
 *************************************************/

App.Router.map(function() {
  this.resource('index', {path:'/'});
  this.resource('program');
  this.resource('designers');
  this.resource('register');
  this.resource('contact');
});

/*************************************************
 * Ember Controllers/Routes 
 *
 *************************************************/
App.ApplicationController = Ember.Controller.extend({
	setTitle:function(title){
  		this.set('title', title);
  	},
	setTagline:function(val){
  		this.set('tagline', val);
  	}
	
});


App.ControllerMixin = {
	hello:'world',
	needs: ['application'],
	getAppController :function(){
		return  this.get('controllers.application');
	}
	
};
App.BaseRoute = Ember.Route.extend({
	setupController:function(controller){		
		controller.getAppController().setProperties(this.headerModel);
	}
});


App.IndexController = Ember.Controller.extend(App.ControllerMixin);
App.IndexRoute = App.BaseRoute.extend({
 	headerModel : {
    	title:'2014  HCI/d  -  CONNECT',
      tagline:'Enjoy a day with our HCI/d master’s students as we learn about your companies, perform design challenges, and share our passions and experiences working within HCI.',
      subheading:'DETAILS',
      subheadingTagline: 'when, where & cost'
	}
});


App.ProgramController = Ember.Controller.extend(App.ControllerMixin);
App.ProgramRoute = App.BaseRoute.extend({
	headerModel : {
    	title:'OUR PROGRAM',
    	tagline:'Indiana University Bloomington’s HCI/d program offers a balanced discourse of design practice, research, prototyping, and theory.'    	,
      subheading:'DISCOURSE',
      subheadingTagline: 'How We Know, What We Know'
	}
});


App.DesignersController = Ember.Controller.extend(App.ControllerMixin);
App.DesignersRoute = App.BaseRoute.extend({
	headerModel : {
    	title:'MEET OUR DESIGNERS',
    	tagline:'Below you will find first & second year HCI/d master’s students. Each designer has a portfolio showcasing their passions and work ! <a class="big-red-button" href="spring-2014-resume-book.pdf">Download Resume Book</a>'    	,
    	subheading:null,
      subheadingTagline: null
  },
  setupController:function(controller){
    this._super(controller);
    controller.set('model', designers);
  }
});


App.RegisterController = Ember.Controller.extend(App.ControllerMixin);
App.RegisterRoute = App.BaseRoute.extend({
	headerModel : {
    	title:'',
    	tagline:''	,
    	subheading:null,
      subheadingTagline: null
  }
});

App.ContactController = Ember.Controller.extend(App.ControllerMixin);
App.ContactRoute = App.BaseRoute.extend({
	headerModel : {
    	title:'CONTACT US',
    	tagline:'Please let us know how we can help!'	,
    	subheading:'CONTACT',
      subheadingTagline: ''
  }
});




/*************************************************
 * Ember Views
 *
 *************************************************/

App.DesignerView = Ember.View.extend({
  click:function(e){
    var el = $('.halo', this.$()).show();
  },
  mouseLeave:function(){
    var el = $('.halo', this.$()).hide();
  },
  className:'designer',
  didInsertElement:function(e){
 }
});
var myhalo;
App.CloseButtonView = Ember.View.extend({
  click:function(e){
    e.stopPropagation();

    var btn = $(e.target);
    myhalo = btn.parents('.designer').find('.halo');
    myhalo.hide();
  }
});



Ember.Handlebars.registerHelper('mod3', function(value, options) {
	alert('hi');
  return (value.id % 3) === 0; 
});


$('.designer .close').bind('click',  function(){
  alert('asdf');
});

